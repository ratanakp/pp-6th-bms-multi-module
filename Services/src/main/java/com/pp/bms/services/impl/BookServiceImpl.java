package com.pp.bms.services.impl;

import com.pp.bms.models.Book;
import com.pp.bms.repositories.BookRepository;
import com.pp.bms.services.BookService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class BookServiceImpl implements BookService {

    private BookRepository bookRepository;

    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> getBooks() {
        return this.bookRepository.getBooks();
    }
}
