package com.pp.bms.services;

import com.pp.bms.models.Book;

import java.util.List;

public interface BookService {

    List<Book> getBooks();



}
