package com.pp.bms.controllers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.pp.bms"})
public class BMSApplication {


    public static void main(String[] args) {
        SpringApplication.run(BMSApplication.class, args);
    }
}
