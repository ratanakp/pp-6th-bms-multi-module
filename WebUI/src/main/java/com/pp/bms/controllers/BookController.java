package com.pp.bms.controllers;

import com.pp.bms.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class BookController {

    private BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }


    @GetMapping("/index")
    public String index(Model model) {
        model.addAttribute("books", this.bookService.getBooks());

        return "index";
    }

}
