package com.pp.bms.repositories;

import com.pp.bms.models.Book;
import com.pp.bms.models.Category;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class BookRepository {


    List<Book> books = new ArrayList<>();


    {
        books.add(new Book(
                1,
                "title",
                "author",
                "publisher",
                "thumbnail",
                new Category(1, "Comics")));
    }


    public List<Book> getBooks() {
        return this.books;
    }

}
